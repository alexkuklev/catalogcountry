package com.example.alex.catalogcountry;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.alex.catalogcountry.model.SubRegionModel;

public class InformationCountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_country);
        SubRegionModel oneCountry = getIntent().getParcelableExtra(SubRegionActivity.SUB_REGION_MODEL);
        TextView nameCountry = findViewById(R.id.name_country);
        TextView countryCapital = findViewById(R.id.value_capital);
        TextView countryPopulation = findViewById(R.id.value_population);

        nameCountry.setText(oneCountry.getNameSubRegion());
        countryCapital.setText(oneCountry.getCapitalSubRegion());
        countryPopulation.setText(oneCountry.getPopulation());

    }
}
