package com.example.alex.catalogcountry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alex.catalogcountry.adapters.SubRegionAdapter;
import com.example.alex.catalogcountry.model.SubRegionModel;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SubRegionActivity extends AppCompatActivity {

    public static final String SUB_REGION_MODEL = "sub_region_model";
    String nameRegion;
    List<SubRegionModel> listSubRegions = new ArrayList<>();
    SubRegionAdapter mAdapter;
    ListView listView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_region);
        nameRegion = getIntent().getStringExtra(MainActivity.NAME_REGION);
        progressBar = findViewById(R.id.progress_bar_sub_region_act);
        getListSubRegion();
        listView = findViewById(R.id.list_sub_regions_view);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SubRegionModel subRegionModel = mAdapter.getItem(position);
                Intent intent = new Intent(SubRegionActivity.this, InformationCountryActivity.class);
                intent.putExtra(SUB_REGION_MODEL, subRegionModel);
                startActivity(intent);
            }
        });
        mAdapter = new SubRegionAdapter(this, listSubRegions);
        listView.setAdapter(mAdapter);
    }

    private void getListSubRegion() {
        Retrofit.getSubRegion(nameRegion, new Callback<List<SubRegionModel>>() {
            @Override
            public void success(List<SubRegionModel> subRegionModels, Response response) {
                listSubRegions = subRegionModels;
                mAdapter.updateList(listSubRegions);
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
