package com.example.alex.catalogcountry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.alex.catalogcountry.R;
import com.example.alex.catalogcountry.model.SubRegionModel;

import java.util.ArrayList;
import java.util.List;

public class SubRegionAdapter extends BaseAdapter {

    private Context context;
    private List<SubRegionModel> subRegions;

    public SubRegionAdapter(Context context, List<SubRegionModel> regions) {
        this.context = context;
        this.subRegions = regions;
    }

    @Override
    public int getCount() {
        return subRegions.size();
    }

    @Override
    public SubRegionModel getItem(int position) {
        return subRegions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder;

        if(rowView == null){
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_regions, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.nameRegion = rowView.findViewById(R.id.name_region_view);

            rowView.setTag(viewHolder);
        }else{
            viewHolder = (SubRegionAdapter.ViewHolder) rowView.getTag();
        }

        viewHolder.nameRegion.setText(subRegions.get(position).getNameSubRegion());
        return rowView;
    }

    public void updateList(List<SubRegionModel> listData) {
        subRegions.addAll(listData);
        notifyDataSetChanged();
    }

    static class ViewHolder{
        TextView nameRegion;
    }
}
