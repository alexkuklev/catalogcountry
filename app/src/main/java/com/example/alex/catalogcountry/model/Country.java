package com.example.alex.catalogcountry.model;

import com.google.gson.annotations.SerializedName;

public class Country {
    @SerializedName("name")
    String name;
    @SerializedName("capital")
    String capital;
    @SerializedName("population")
    long population;

    @Override
    public String toString() {
        return name + "\n" + capital + "\n" + population;
    }
}
