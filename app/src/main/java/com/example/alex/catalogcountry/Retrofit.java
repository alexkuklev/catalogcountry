package com.example.alex.catalogcountry;

import com.example.alex.catalogcountry.model.Country;
import com.example.alex.catalogcountry.model.SubRegionModel;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public class Retrofit {
    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface{

        @GET("/v2/region/{name}")
        void getSubRegions(@Path("name") String countryName,
                          Callback<List<SubRegionModel>> callback);


        @GET("/v2/name/{name}")
        void getCountries(@Path("name") String countryName,
                          @Query("fullText") String fullText,
                          Callback<List<Country>> callback);
    }

    private static void initialize(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCountries(String nameCountry, Callback<List<Country>> callback){
        apiInterface.getCountries(nameCountry, "true", callback);
    }

    public static void getSubRegion(String nameRegion, Callback<List<SubRegionModel>> callback){
        apiInterface.getSubRegions(nameRegion, callback);
    }
}
