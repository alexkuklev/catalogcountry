package com.example.alex.catalogcountry.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alex.catalogcountry.R;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private String[] regions;

    public MyAdapter(Context context, String[] regions) {
        this.context = context;
        this.regions = regions;
    }

    @Override
    public int getCount() {
        return regions.length;
    }

    @Override
    public String getItem(int position) {
        return regions[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder;

        if(rowView == null){
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_regions, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.nameRegion = rowView.findViewById(R.id.name_region_view);

            rowView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) rowView.getTag();
        }

        viewHolder.nameRegion.setText(regions[position]);
        return rowView;
    }

    static class ViewHolder{
        TextView nameRegion;
    }
}
