package com.example.alex.catalogcountry.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SubRegionModel implements Parcelable {
    @SerializedName("name")
    private String nameSubRegion;
    @SerializedName("capital")
    private String capitalSubRegion;
    @SerializedName("population")
    private String populationSubRegion;

    protected SubRegionModel(Parcel in) {
        nameSubRegion = in.readString();
        capitalSubRegion = in.readString();
        populationSubRegion = in.readString();
    }

    public static final Creator<SubRegionModel> CREATOR = new Creator<SubRegionModel>() {
        @Override
        public SubRegionModel createFromParcel(Parcel in) {
            return new SubRegionModel(in);
        }

        @Override
        public SubRegionModel[] newArray(int size) {
            return new SubRegionModel[size];
        }
    };

    public String getNameSubRegion() {
        return nameSubRegion;
    }
    public String getCapitalSubRegion() {
        return capitalSubRegion;
    }
    public String getPopulation() {
        return populationSubRegion;
    }

    @Override
    public String toString() {
        return nameSubRegion + "\n";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameSubRegion);
        dest.writeString(capitalSubRegion);
        dest.writeString(populationSubRegion);
    }
}
