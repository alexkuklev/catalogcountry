package com.example.alex.catalogcountry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.alex.catalogcountry.adapters.MyAdapter;

public class MainActivity extends AppCompatActivity {

    //List<Country> mcountries = new ArrayList<>();
    //List<User> listUser = new ArrayList<>();//
    public static final String NAME_REGION = "nameRegion";

    String[] regionsArray = {"Africa", "Americas", "Asia", "Europe", "Oceania"};
    MyAdapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listView = findViewById(R.id.list_regions_view);
        myAdapter = new MyAdapter(this, regionsArray);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, SubRegionActivity.class);
                intent.putExtra(NAME_REGION, regionsArray[position]);
                startActivity(intent);
            }
        });
        listView.setAdapter(myAdapter);


    }
}
